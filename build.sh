#!/usr/bin/env sh
set -e
rm -rf tmp/
rm -rf icons/
mkdir tmp/
curl -LqsS https://game-icons.net/archives/000000/transparent/game-icons.net.svg.zip -o tmp/game-icons.zip
unzip tmp/game-icons.zip -d tmp >/dev/null
mv tmp/icons/000000/transparent/1x1/ icons
cp -R local-icons/* icons/
npm install
npm run build-font
