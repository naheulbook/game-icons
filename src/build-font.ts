const fs = require('fs');
const util = require('util');
const SVGIcons2SVGFontStream = require('svgicons2svgfont');
const svg2ttf = require('svg2ttf');
const ttf2woff2 = require('ttf2woff2');

async function listRecursiveFiles(path: string, filesList: string[]): Promise<void> {
    const directories = await util.promisify(fs.readdir)(path, {withFileTypes: true});
    for (const directory of directories) {
        const childPath = path + '/' + directory.name;
        if (directory.isDirectory()) {
            await listRecursiveFiles(childPath, filesList);
        } else {
            filesList.push(childPath);
        }
    }
}

async function listFiles(path: string): Promise<string[]> {
    const filesList: string[] = [];
    await listRecursiveFiles(path, filesList);
    return filesList.map(filePath => filePath.replace(path + '/', ''));
}

function iconNameFromFilePath(filePath: string) {
    let iconName = filePath.replace('.svg', '');
    iconName = iconName.substr(iconName.lastIndexOf('/') + 1);
    return iconName;
}

function convertToTtfAndWoff2(fontFile: string, timestamp?: number) {
    const ttf = svg2ttf(fs.readFileSync(fontFile, 'utf8'), {ts: timestamp});
    let ttfFilePath = fontFile.replace('.svg', '.ttf');
    fs.writeFileSync(ttfFilePath, new Buffer(ttf.buffer));
    let woff2Path = ttfFilePath.replace('.ttf', '.woff2');
    fs.writeFileSync(woff2Path, ttf2woff2(new Buffer(ttf.buffer)));
}

export interface IconInfo {
    unicode: number;
    filePath: string;
    name: string;
    timestamp?: number;
}
async function createFont(chunkId: number, icons: IconInfo[]): Promise<void> {

    const fontStream = new SVGIcons2SVGFontStream({
        fontName: 'game-icons',
        normalize: true,
        fontHeight: 1024,
        descent: 128,
        round: 10e2
    });

    return new Promise((resolve, reject) => {
       const fontPath = 'fonts/game-icons-' + chunkId+ '.svg';
        fontStream.pipe(fs.createWriteStream(fontPath))
            .on('finish', function () {
                const timestamp = icons.reduce((ts: number | undefined, icon) => icon.timestamp && icon.timestamp > (ts || 0) ? icon.timestamp : ts, undefined);
                convertToTtfAndWoff2(fontPath, timestamp);
                fs.unlinkSync(fontPath);
                resolve();
            })
            .on('error', function (err) {
                console.log(err);
                reject(err);
            });

        for (let icon of icons) {
            const glyph = fs.createReadStream('icons/' + icon.filePath);
            glyph.metadata = {
                unicode: [String.fromCodePoint(icon.unicode)],
                name: icon.name
            };
            fontStream.write(glyph);
        }

        fontStream.end();
    });
}
function toCssRange(minUnicode: number, maxUnicode: number) {
    return 'U+' + minUnicode.toString(16).toUpperCase() + '-' + maxUnicode.toString(16).toUpperCase()
}

async function createCssFile(iconsByChunk: { [index: number]: IconInfo[] }) {
    let indexHtmlContent = `<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Test</title>
        <link rel="stylesheet" type="text/css" href="game-icons.css">
    </head>
    <body>\n`;

    for (const chunkId of Object.keys(iconsByChunk).map(c => +c).sort((a, b) => a - b)) {
        const icons = iconsByChunk[chunkId];
        indexHtmlContent += '   <div>\n';
        for (const icon of icons) {
            indexHtmlContent += `        <span class="game-icon game-icon-${icon.name}"></span>\n`;
        }
        indexHtmlContent += '   </div><hr>\n';
    }

    indexHtmlContent += `</body>\n</html>\n`;

    await util.promisify(fs.writeFile)('./fonts/index.html', indexHtmlContent);

}
async function createIndexFile(iconsByChunk: { [index: number]: IconInfo[] }) {

    let cssFileContent = '';
    for (const chunkId of Object.keys(iconsByChunk).map(c => +c).sort((a, b) => a - b)) {
        const icons = iconsByChunk[chunkId];
        cssFileContent += `@font-face {
    font-family: "game-icons";
    font-weight: normal;
    font-style: normal;
    src: url('./game-icons-${chunkId}.ttf');
    src: url('./game-icons-${chunkId}.woff2') format('woff2'),
        url('./game-icons-${chunkId}.ttf') format('truetype');
    unicode-range: ${toCssRange(icons[0].unicode, icons[icons.length - 1].unicode)}, U+20;
}\n`;
    }

    cssFileContent += `
.game-icon {
    font-family: "game-icons";
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 1;
    speak: none;
    text-transform: none;
}`;

    for (const chunkId of Object.keys(iconsByChunk).map(c => +c).sort((a, b) => a - b)) {
        const icons = iconsByChunk[chunkId];

        for (const icon of icons) {
            cssFileContent += `
.game-icon-${icon.name}:before {
    content: "\\${icon.unicode.toString(16).toUpperCase()}";
}`
        }
    }
    await util.promisify(fs.writeFile)('./fonts/game-icons.css', cssFileContent);
}


async function main(): Promise<void> {
    const startCode = 0xFF000;
    const iconsPerChunk = 128;
    let iconsInfo: {
        icons: {[fileName: string]: IconInfo},
        namesCount: {[name: string]: number},
        codeIncr: number
    }= {icons: {}, namesCount: {}, codeIncr: startCode};
    const filesList = (await listFiles('icons')).sort();

    if (fs.existsSync('./fonts/icons-info.json')) {
        const configJson = fs.readFileSync('./fonts/icons-info.json');
        iconsInfo = JSON.parse(configJson);
    }

    for (let fileName of filesList) {
        if (!(fileName in iconsInfo.icons)) {
            let name = iconNameFromFilePath(fileName);
            if (name in iconsInfo.namesCount) {
                const number = ++iconsInfo.namesCount[name];
                name += '-' + number;
            } else {
                iconsInfo.namesCount[name] = 1
            }
            iconsInfo.icons[fileName] = {
                unicode: iconsInfo.codeIncr++,
                name: name,
                filePath: fileName,
                timestamp: (new Date().getTime() / 1000)
            };
        } else {
            if (!iconsInfo.icons[fileName].timestamp) {
                iconsInfo.icons[fileName].timestamp = Math.round(new Date().getTime() / 1000);
            }
        }
    }


    const allIcons = Object.keys(iconsInfo.icons)
        .map(key => ({...iconsInfo.icons[key], filePath: key}))
        .sort((a: IconInfo, b: IconInfo) => a.unicode - b.unicode);

    const iconsByChunk = allIcons.reduce((chunkedIcons: {[index: number]: IconInfo[]}, icon: IconInfo) => {
        const chunkIndex = Math.floor((icon.unicode - startCode) / iconsPerChunk);
        if (!chunkedIcons[chunkIndex]) {
            chunkedIcons[chunkIndex] = [];
        }
        chunkedIcons[chunkIndex].push(icon);
        return chunkedIcons;
    }, {});

    for (const chunkId of Object.keys(iconsByChunk).sort()) {
        await createFont(+chunkId, iconsByChunk[chunkId]);
    }

    await createCssFile(iconsByChunk);
    await createIndexFile(iconsByChunk);

    fs.writeFileSync('./fonts/icons-info.json', JSON.stringify(iconsInfo));
    fs.writeFileSync('./fonts/icons-list.json', JSON.stringify(Object.values(iconsInfo.icons).map(x => x.name), null, ' '));



}

main().then();
